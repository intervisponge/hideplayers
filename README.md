# HidePlayers

Plugin for hide players in server. Features:

* set players invisible
* hide in tab
* hide join, leave, death messages
* no chatting (messages not sent)
* hide grant advancement messages
* hide kick messages

## Commands

* **/hpl-reload** - reload config (need *hideplayers.cmd.reload* permission)
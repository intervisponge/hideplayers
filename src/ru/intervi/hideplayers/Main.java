package ru.intervi.hideplayers;

import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.tab.TabList;
import org.spongepowered.api.entity.living.player.tab.TabListEntry;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.advancement.AdvancementEvent;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.entity.living.humanoid.player.KickPlayerEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

@Plugin(id = "hideplayers", name = "HidePlayers", version = "1.0", description = "Hide players in tab and messages.")
public class Main implements CommandExecutor {
    @Inject
    @DefaultConfig(sharedRoot = true)
    private Path defaultConfig;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    private CommentedConfigurationNode config;

    private void setupConfig(){
        try {
            if (!Files.exists(defaultConfig)) {
                Files.createFile(defaultConfig);
                config = configManager.createEmptyNode();
                config.getNode("invisible").setValue(true).setComment("Set players is invisible for all.");
                config.getNode("hidetab").setValue(true)
                        .setComment("Hide players in tab, show only this user (like a empty server).");
                config.getNode("hidechat").setValue(true).setComment("Blocking chatting.");
                config.getNode("hidemessages").setValue(true).setComment("Hide join, leave, death messages.");
                config.getNode("hidegrant").setValue(true).setComment("Hide grant advancement messages.");
                config.getNode("hidekick").setValue(true).setComment("Hide kick messages.");
                configManager.save(config);
            } else {
                config = configManager.load();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        setupConfig();
        CommandSpec reload = CommandSpec.builder()
                .description(Text.of("Config reload command."))
                .permission("hideplayers.cmd.reload")
                .executor(this)
                .build();
        Sponge.getCommandManager().register(this, reload, "hpl-reload", "hideplayers");
    }

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        Player player = event.getTargetEntity();
        if (config.getNode("invisible").getBoolean()) {
            player.offer(Keys.INVISIBLE, true);
            player.offer(Keys.VANISH, true);
            player.offer(Keys.VANISH_IGNORES_COLLISION, true);
            player.offer(Keys.VANISH_PREVENTS_TARGETING, true);
        }
        if (config.getNode("hidetab").getBoolean()) {
            TabList tab = player.getTabList();
            UUID playerId = player.getUniqueId();
            for (TabListEntry entry : tab.getEntries()) {
                UUID uuid = entry.getProfile().getUniqueId();
                if (uuid == playerId) continue;
                tab.removeEntry(uuid);
            }
            for (Player p : Sponge.getGame().getServer().getOnlinePlayers()) {
                if (p.getUniqueId() == playerId) continue;
                p.getTabList().removeEntry(player.getUniqueId());
            }
        }
        if (config.getNode("hidemessages").getBoolean()) {
            event.setMessageCancelled(true);
        }
    }

    @Listener
    public void onLeave(ClientConnectionEvent.Disconnect event) {
        if (config.getNode("hidemessages").getBoolean()) {
            event.setMessageCancelled(true);
        }
    }

    @Listener
    public void onDeath(DestructEntityEvent.Death event) {
        if (config.getNode("hidemessages").getBoolean()) {
            event.setMessageCancelled(true);
        }
    }

    @Listener
    public void onChat(MessageChannelEvent.Chat event) {
        if (config.getNode("hidechat").getBoolean()) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onGrant(AdvancementEvent.Grant event) {
        if (config.getNode("hidegrant").getBoolean()) {
            event.setMessageCancelled(true);
        }
    }

    @Listener
    public void onKick(KickPlayerEvent event) {
        if (config.getNode("hidekick").getBoolean()) {
            event.setMessageCancelled(true);
        }
    }

    @Nonnull
    @Override
    public CommandResult execute(@Nonnull  CommandSource src, @Nonnull CommandContext args) throws CommandException {
        setupConfig();
        src.sendMessage(Text.of("config reloaded"));
        return CommandResult.success();
    }
}
